import os
import numpy as np
import tensorflow as tf
from utils.utils import save_images, make_dirs
from utils.metrics import acc, nmi, ari

class Experiments(object):
    def __init__(self, sess, config, data, model):
        self.sess = sess
        self.config = config
        self.data = data
        self.model = model

        # directory to save experimental results
        self.dir = make_dirs(os.path.join(self.config.result_path, self.config.dataset))

    def image_generation(self):

        # the number of samples
        nx = ny = 10
        num_samples = nx * ny
        # sampling z from N(0, 1) prior
        z_cont = np.random.normal(0, 1, (num_samples, self.config.cont_dim))
        z_disc = np.vstack([np.eye(nx, 10)] * ny)
        z_sample = np.concatenate([z_cont, z_disc], 1)
        # generate images
        samples = self.sess.run(self.model.generated_images, feed_dict={self.model.input_z: z_sample})
        # save images
        save_images(samples, make_dirs(os.path.join(self.dir, 'image_generation')) + '/epoch_{}'.format(self.model.epoch_counter.eval(self.sess)) + '.png')

    def latent_traverse(self):
        pass

    def clustering(self):
        x_train, x_test = self.data.x_train, self.data.x_test
        y_train, y_test = self.data.y_train, self.data.y_test
        alpha = self.sess.run(self.model.alpha, feed_dict={self.model.input_x: x_test})
        y_pred = np.argmax(alpha, axis=1)
        print(acc(y_test, y_pred))


