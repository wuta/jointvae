import os
import tensorflow as tf
from utils.utils import make_dirs

class BaseModel(object):
    def __init__(self, sess, config):
        self.sess = sess
        self.config = config
        self.image_dim = [32, 32, 3]
        if self.config.dataset in ['mnist', 'fashion_mnist']:
            # resize form [28, 28] to [32, 32]
            self.image_dim = [32, 32, 1]

        # init the epoch and global step counters
        self.init_counter()

        # init saver
        self.saver = tf.train.Saver(max_to_keep=self.config.max_to_keep)
        self.checkpoint_dir = make_dirs(os.path.join(self.config.checkpoint_path, self.config.dataset))

    # here build different models for projects
    def build_model(self):
        raise NotImplementedError

    # save function that saves the checkpoint in the path defined in the config file
    def save_model(self):
        print("Saving model...")
        self.saver.save(self.sess, self.checkpoint_dir + '/ckpt', self.global_step)
        print("Model saved")

    # load latest checkpoint from the experiment path defined in the config file
    def load_model(self):
        latest_checkpoint = tf.train.latest_checkpoint(self.checkpoint_dir)
        if latest_checkpoint:
            print("Loading model checkpoint {} ...\n".format(latest_checkpoint))
            self.saver.restore(self.sess, latest_checkpoint)
            print("Loading model successed, model has been trained for {} epochs".format(self.epoch_counter.eval(self.sess)))
        else:
            print("Loading model failed")

    # just initialize a tensorflow variable to use it as epoch/global_step counter
    def init_counter(self):
        with tf.variable_scope('epoch'):
            self.epoch_counter = tf.Variable(0, trainable=False, name='epoch_counter')
            self.increment_epoch = tf.assign(self.epoch_counter, self.epoch_counter + 1)

        with tf.variable_scope('global_step'):
            self.global_step = tf.Variable(0, trainable=False, name='global_step_counter')
            self.sess.run(self.global_step.initializer)
            self.increment_global_step = tf.assign(self.global_step, self.global_step + 1)

