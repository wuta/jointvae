import tensorflow as tf

class BaseTrain(object):
    def __init__(self, sess, model, data, config, logger, experiment):
        self.sess = sess
        self.model = model
        self.data = data
        self.config = config
        self.logger = logger
        self.experiment = experiment

        self.init = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
        self.sess.run(self.init)

    def train(self):
        # restore trained model if exists
        #self.model.load_model()

        for epoch in range(self.model.epoch_counter.eval(self.sess), self.config.num_epochs + 1):
            self.train_epoch()
            self.sess.run(self.model.increment_epoch)

    def train_epoch(self):
        """
        implement the logic of training epoch

        """
        raise NotImplementedError

    def train_step(self):
        """
        implement the logic of the train step

        """
        raise NotImplementedError
