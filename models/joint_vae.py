import numpy as np
import tensorflow as tf
from base.base_model import BaseModel

# samll number used for numerical stability
EPS = 1e-8

class JointVAE(BaseModel):
    def __init__(self, sess, config):
        super(JointVAE, self).__init__(sess, config)
        self.build_model()

    def encoder(self, x, reuse=False):
        with tf.variable_scope('encoder', reuse=reuse):
            conv = x
            for filters, kernel_size, strides in self.config.encoder["conv"]:
                conv = tf.layers.conv2d(inputs=conv,
                                        filters=filters,
                                        kernel_size=kernel_size,
                                        strides=strides,
                                        padding='same',
                                        activation=tf.nn.relu)
            fc = tf.layers.flatten(conv)
            for hidden_units in self.config.encoder["fc"]:
                fc = tf.layers.dense(inputs=fc,
                                     units=hidden_units,
                                     activation=tf.nn.relu)

            # output layer
            with tf.variable_scope('continuous', reuse=reuse):
                # The mean and log_var for continuous Gaussian distribution
                mean = tf.layers.dense(fc, units=self.config.cont_dim)
                log_var = tf.layers.dense(fc, units=self.config.cont_dim)

            # output layer
            with tf.variable_scope('discrete', reuse=reuse):
                # The mean and log_var for continuous Gaussian distribution
                alpha = tf.layers.dense(fc, units=self.config.disc_dim, activation=tf.nn.softmax)

            return mean, log_var, alpha

    def decoder(self, z, reuse=False):
        with tf.variable_scope('decoder', reuse=reuse):
            fc = z
            for hidden_units in self.config.decoder["fc"]:
                fc = tf.layers.dense(inputs=fc,
                                     units=hidden_units,
                                     activation=tf.nn.relu)
            conv = tf.reshape(fc, [-1, 4, 4, 64])
            for filters, kernel_size, strides in self.config.decoder["conv"][:-1]:
                conv = tf.layers.conv2d_transpose(inputs=conv,
                                                  filters=filters,
                                                  kernel_size=kernel_size,
                                                  strides=strides,
                                                  padding='same',
                                                  activation=tf.nn.relu)
            filters, kernel_size, strides = self.config.decoder["conv"][-1]
            out = tf.layers.conv2d_transpose(conv,
                                             filters=filters,
                                             kernel_size=kernel_size,
                                             strides=strides,
                                             padding='same',
                                             activation=tf.nn.sigmoid)
            return out

    def build_model(self):

        # placeholder
        self.z_dim = self.config.cont_dim + self.config.disc_dim
        self.input_x = tf.placeholder(dtype=tf.float32, shape=[None] + self.image_dim, name='input_x')
        self.input_z = tf.placeholder(dtype=tf.float32, shape=[None, self.z_dim], name='input_z')

        # encoding
        self.mean, self.logvar, self.alpha = self.encoder(self.input_x, reuse=False)
        cont_z = self._repa_normal(self.mean, self.logvar)
        disc_z = self._repa_gumbel_softmax(self.alpha)
        self.z = tf.concat([cont_z, disc_z], 1)

        # decoding 
        self.outputs = self.decoder(self.z, reuse=False)

        # testing 
        self.generated_images = self.decoder(self.input_z, reuse=True)

        self.loss, self.optim = self.loss_func()


    def loss_func(self):

        # loss
        with tf.variable_scope("loss"):
            self.step = tf.placeholder(dtype=tf.float32, shape=(), name='step')
            # reconstruction loss term
            self.rec_loss = -tf.reduce_mean(tf.reduce_sum(self.input_x * tf.log(EPS + self.outputs) + 
                                                         (1 - self.input_x) * tf.log(EPS + 1 - self.outputs), axis=[1, 2, 3]))

            # Gaussian KL divergence loss term
            kl_normal = tf.reduce_mean(0.5 * tf.reduce_sum(tf.square(self.mean) + tf.exp(self.logvar) - self.logvar - 1, axis=[1]))
            cont_cap_min, cont_cap_max, cont_schedule_steps, cont_gamma = self.config.cont_capacity
            cont_cap_current = (cont_cap_max- cont_cap_min) / cont_schedule_steps * self.step + cont_cap_min
            self.cont_cap_current = tf.minimum(cont_cap_max, cont_cap_current)
            self.cont_loss = cont_gamma * tf.abs(self.cont_cap_current - kl_normal)

            # Discrete KL divergence loss term
            kl_disc = tf.reduce_mean(tf.reduce_sum(self.alpha * (tf.log(self.alpha + EPS) - tf.log(1.0 / self.config.disc_dim)), axis=[1]))
            disc_cap_min, disc_cap_max, disc_schedule_steps, disc_gamma = self.config.disc_capacity
            disc_cap_current = (disc_cap_max- disc_cap_min) / disc_schedule_steps * self.step + disc_cap_min
            disc_cap_current = tf.minimum(disc_cap_max, disc_cap_current)
            self.disc_loss = disc_gamma * tf.abs(disc_cap_current - kl_disc)

            loss = self.rec_loss + self.cont_loss + self.disc_loss
    
        # optimizer
        with tf.variable_scope("optimizer"):
            t_vars = tf.trainable_variables()
            with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
                optim = tf.train.AdamOptimizer(self.config.learning_rate).minimize(loss, var_list=t_vars)

        return loss, optim

    def _repa_normal(self, mean, logvar):
        std = tf.exp(0.5 * logvar)
        epsilon = tf.random_normal(tf.shape(std), dtype=tf.float32)
        return mean + std * epsilon

    def _repa_gumbel_softmax(self, alpha):
        unif = tf.random_uniform(tf.shape(alpha), dtype=tf.float32)
        gumbel = -tf.log(-tf.log(unif + EPS) + EPS)
        logits = (tf.log(alpha + EPS) + gumbel) / self.config.temperature
        return tf.nn.softmax(logits, 1)

