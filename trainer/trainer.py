import numpy as np
from tqdm import tqdm
import tensorflow as tf
from base.base_train import BaseTrain

# samll number used for numerical stability
EPS = 1e-8

class Trainer(BaseTrain):
    def __init__(self, sess, model, data, config, logger, experiment):
        super(Trainer, self).__init__(sess, model, data, config, logger, experiment)
    
    def train_step(self):
        batch_x = self.data.get_batch(self.data.x_train, self.config.batch_size)
        feed_dict = {self.model.input_x: batch_x, self.model.step: self.model.global_step.eval(self.sess)}
        _, loss = self.sess.run([self.model.optim, self.model.loss], feed_dict=feed_dict)
        self.model.increment_global_step.eval(session=self.sess)
        return loss

    def train_epoch(self):
        steps = tqdm(range(len(self.data.x_train) // self.config.batch_size))
        for step in steps:
            loss = self.train_step()
            if step % 100 == 0:
                print('epoch:{}, iteration:{}, loss:{}'.format(self.model.epoch_counter.eval(self.sess), step, loss))

        # show experimental results eevery epoch
        self.experiment.image_generation()
        self.experiment.clustering()
        # save log file for tensorboard visualization every epoch
        summaries_dict = {'loss': loss}
        self.logger.summarize(self.model.global_step.eval(self.sess), summaries_dict=summaries_dict)
        # save model every epoch
        self.model.save_model()

