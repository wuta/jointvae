import numpy as np
from sklearn.utils.linear_assignment_ import linear_assignment
from sklearn.metrics import normalized_mutual_info_score
from sklearn.metrics import adjusted_rand_score

def acc(Y_true, Y_pred):
    assert Y_pred.size == Y_true.size
    D = max(Y_pred.max(), Y_true.max())+1
    w = np.zeros((D,D), dtype=np.int64)
    for i in range(Y_pred.size):
        w[Y_pred[i], Y_true[i]] += 1
    ind = linear_assignment(w.max() - w)
    return sum([w[i,j] for i,j in ind])*1.0/Y_pred.size

def nmi(Y_true, Y_pred):
    return normalized_mutual_info_score(Y_true, Y_pred)

def ari(Y_true, Y_pred):
    return adjusted_rand_score(Y_true, Y_pred)
