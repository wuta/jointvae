import numpy as np
import tensorflow as tf
import tensorflow_datasets as tfds

class DataLoader(object):
    def __init__(self, sess, config):
        self.sess = sess
        self.config = config
        # load data
        dataset = tfds.load(self.config.dataset, data_dir='data_loader', batch_size=-1)
        # transfer to numpy type
        train_data, test_data = tfds.as_numpy(dataset['train']), tfds.as_numpy(dataset['test'])
        x_train, x_test = train_data['image'], test_data['image']	
        self.y_train, self.y_test = train_data['label'], test_data['label']
         
        # normalizing
        self.x_train, self.x_test = x_train / 255.0, x_test / 255.0
        if self.config.dataset in ['mnist', 'fashion_mnist']:
            self.x_train = tf.image.resize_images(self.x_train, (32, 32)).eval(session=self.sess)
            self.x_test = tf.image.resize_images(self.x_test, (32, 32)).eval(session=self.sess)

    def get_batch(self, data, batch_size):
        idx = np.random.choice(len(data), batch_size)
        return data[idx]
