#!/usr/bin/python3
import argparse
import tensorflow as tf
from data_loader.data_loader import DataLoader
from models.joint_vae import JointVAE
from trainer.trainer import Trainer
from utils.logger import Logger
from experiments.experiments import Experiments
from utils.utils import get_config_from_json

def parse_args():
    desc = "Tensorflow implementation of JointVAE(https://arxiv.org/abs/1804.00104)"
    argparser = argparse.ArgumentParser(description=desc)
    argparser.add_argument('-c', '--config', default='configs/mnist.json', help='The path of configuration file')
    args = argparser.parse_args()

    return args

def main():

    args = parse_args()
    config = get_config_from_json(args.config)

    with tf.Session() as sess:

        # create your data generator
        data = DataLoader(sess, config)
        # create an instance of the model you want
        model = JointVAE(sess, config)
        # create tensorboard logger
        logger = Logger(sess, config, model)
        # create experiments
        experiment = Experiments(sess, config, data, model)
        # create trainer and pass all the previous components to it
        trainer = Trainer(sess, model, data, config, logger, experiment)
        # here you train your model
        trainer.train()
    
if __name__ == '__main__':
    main()
